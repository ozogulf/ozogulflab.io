---
title: "Hızlıca Docker Komutları"
date: 2022-10-17T12:00:06+09:00
description: "Docker CheatSheet"
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tags: 
- docker
---

Docker üzerinde çalışırken çoğu zaman farklı komutlara ihtiyaç duyuyorum ve her seferinde bu komutları tekrar tekrar araştırmak zorunda kalıyorum.
Bu nedenle sık kullandığım ve nairen de olsa ihtiyacım olan kilit komutlara burada yer vermekte fayda olduğunu düşündüm.

Muhtemelen bu içerik ilerleyen zamanlarda güncellenecektir.

### Log

Zaman zaman ihtiyaç halinde ortaya çıkan bazı kullanışkı docker komutlarının bir derlemesi;

Konteynırlara ait run time loglarını listelemek için;

``` bash
docker logs <container_id/container_name>
```

Komuta ```--follow``` parametrasi verilirse çıktılar canlı olarak izlenebilir.

Bir konteynıra ait logları temizlemek için ise;
``` bash
sudo sh -c 'echo "" > $(docker inspect --format="{{.LogPath}}" <container_id/container_name>)'
```

### Network

Var olan docker ağlarını listelemek için;
``` bash 
docker network ls
```
Hangi ağın hangi containerlar tarafından kullanıldığını listelemek için;
``` bash
docker network inspect <network_name>
```

Çalışan bir konteynırda komut işletmek için;
``` bash
docker exec <container_id/container_name>  <command>
```

Çalışan bir konteynırda bash erişimi için;
``` bash
docker exec <container_id/container_name> bash
```

### Kopyalama

dockerdan locale;

``` bash
docker cp <container_id/container_name>:docker_source_path local_destination_path
```

Localden dockera;

``` bash
docker cp local_source_path <container_id/container_name>:docker_destination_path
```