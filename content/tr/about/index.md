+++
title = "hakkımda"
description = "Kişisel Bloguma Hoşgeldiniz"
type = "about"
date = "2022-09-13"
+++

Merhaba, 
Ben Furkan.
Teknik ve gündelik yaşantımda birçok konuya ilgi duymakta ve zaman zaman bu konular hakkında öğrenmeye, çalışmalar yapmaya uğraşıyorum.
Bu arada evet bilgisayar mühendisiyim :). Bu blogta da muhtemelen daha çok yazılım geliştirme, iot, linux vb hakkında içerikler bulacaksınız.
Çünkü bu blogun temel amacı kendime notlar tutmak.

Kısa bir özet geçmek gerekirse;

* Ağustos, 2017
        Kocaeli Üniversitesi Bilgisayar Mühendisliğinden mezun oldum.
* Mayıs, 2018
        Bilkon şirketinde iş hayatına atıldım. Burada gömülü sistemler üzerinde yazılım geliştirme ve bir takım Dev-Ops faaliyetlerinde bulundum.
* Aralık, 2019
        Sparse Technology şirketine geçiş yaptım ve gömülü sistemler üzerinde çalışmalarıma devam ettim.

## ilgi Alanları/bilinenler

Aşağıdaki konularla yakından ilgileniyorum, bu blogda çoğunlukla bu konular ile ilgili yazılar bulacaksınız.

* Docker, CI/CD,

* cpp, Qt Framework,

* CMake, Git, Jira, Vi/Vim

* Arduino, Raspberry Pi vb kartlar üzerinde IoT uygulamaları,

* Rs-232/422/485, Tcp, Udp, gRpc vb haberleşme protokelleri,
